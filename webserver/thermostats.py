import asyncio
import re

import tornado.web
from tornado.iostream import StreamClosedError

from brewserver.config import Config
from brewserver.data import Thermostat

def thermostat_dict(thermostat):
     return {
                'thermostat': thermostat.name,
                'targetSensor': thermostat.target_sensor,
                'targetTemp': thermostat.target_temp,
                'limitSensor': thermostat.limit_sensor,
                'limitTemp': thermostat.limit_temp,
                'threshold': thermostat.threshold,
                'status': thermostat.status,
                'mode': thermostat.mode,
                'lastUpdated': thermostat.last_updated.strftime('%c')
            }

class ThermostatHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.conf = Config()

    def list_thermostats(self):
        self.finish({'thermostats': self.conf.thermostat_names})

    def get_thermostat(self, name):
        t = Thermostat(name)
        data = thermostat_dict(t)
        t.close()
        self.set_header('Cache-Control', 'no-store max-age=0')
        self.finish(data)

    def config_thermostat(self, path):
        m = re.match(r'^([\w]+)/(mode|target-temp|limit-temp)/([\w.]+)$', path)
        
        if m is None or m[1] not in self.conf.thermostat_names:
            self.send_error(400)
            return
        therm_name = m[1]
        attr = m[2].replace('-','_')
        val = m[3]

        if attr in ('target_temp','limit_temp'):
            val = float(val)       

        t = Thermostat(therm_name)
        setattr(t, attr, val)
        t.close()

        set_config = getattr(self.conf, f'set_thermostat_{attr}')
        set_config(therm_name, val)

        self.set_status(200)

    async def stream_thermostats(self):
        self.set_header('Cache-Control', 'no-cache')

        therms = []
        for tn in self.conf.thermostat_names:
            therms.append(Thermostat(tn))
        while True:
            try:
                self.write('data: ')
                self.write({'thermostatData': [thermostat_dict(t) for t in therms ]})
                self.write('\n\n')
                self.set_header('Content-Type', 'text/event-stream')
                await self.flush()
                await asyncio.sleep(1)
            except StreamClosedError:
                for t in therms: t.close()
                break

    async def get(self, func, path):
        conf = Config()
        if func == 'list' or func is None:
            self.list_thermostats()
        elif func == 'get':
            if re.match(r'^[\w]+$', path) is not None:
                if path not in conf.thermostat_names:
                    self.send_error(400)
                    return
                self.get_thermostat(path)
            else:
                self.send_error(400)
        elif func == 'stream':
            await self.stream_thermostats()
        else:
            self.send_error(400)
        

    def put(self, func, path):
        if func == 'config':
            self.config_thermostat(path)
        else:
            self.send_error(400)