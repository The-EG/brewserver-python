import tornado.web
import inspect
import os

def Server():
    from webserver.sensors import SensorHandler
    from webserver.thermostats import ThermostatHandler

    return tornado.web.Application([
        (r'/sensors/([\w]+)/?(.*)', SensorHandler),
        (r'/thermostats/([\w]+)/?(.*)', ThermostatHandler)
    ])