import asyncio
import re

import tornado.web
from tornado.iostream import StreamClosedError

from brewserver.config import Config
from brewserver.data import Sensor

def sensor_dict(sensor):
    return {
            'sensor': sensor.name,
            'tempC': sensor.temp_c,
            'tempF': sensor.temp_f,
            'lastUpdated': sensor.last_updated.strftime('%c')
        }

class SensorHandler(tornado.web.RequestHandler):

    def initialize(self):
        self.conf = Config()

    def list_sensors(self):
        self.finish({'sensors': self.conf.sensor_names})

    def get_sensor(self, name):
        s = Sensor(name)
        data = sensor_dict(s)
        s.close()
        self.set_header('Cache-Control', 'no-store max-age=0')
        self.finish(data)

    async def stream_sensors(self):
        self.set_header('Cache-Control', 'no-cache')

        sensors = []
        for sn in self.conf.sensor_names:
            sensors.append(Sensor(sn))
        while True:
            try:
                self.write('data: ')
                self.write({'sensorData': [sensor_dict(s) for s in sensors ]})
                self.write('\n\n')
                self.set_header('Content-Type', 'text/event-stream')
                await self.flush()
                await asyncio.sleep(1)
            except StreamClosedError:
                for s in sensors: s.close()
                break

    async def get(self, func, path):
        if func == 'list':
            self.list_sensors()
        elif func == 'get':
            if re.match(r'^[\w]+$', path) is not None:
                if path not in self.conf.sensor_names:
                    self.send_error(400)
                    return
                self.get_sensor(path)
            else:
                self.send_error(400)
        elif func == 'stream':
            await self.stream_sensors()	

