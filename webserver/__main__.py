import tornado.ioloop

from webserver import Server

app = Server()
app.listen(8080)
tornado.ioloop.IOLoop.current().start()
