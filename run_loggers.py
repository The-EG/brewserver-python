#!/usr/bin/python
# -*- coding: utf-8 -*-
import configparser
import time
from brewserver import logger

if __name__=='__main__':
    config = configparser.ConfigParser()
    config.read('/mnt/pidata/brewserver/brewserver.ini')
    sensors = []
    for section in config.sections():
        if section.upper().startswith('SENSOR='):
            sname = section[7:]
            print(f'Starting logging on {sname}...')
            s = logger.SensorLogger(sname)
            s.start()
            sensors.append(s)
    
    while True:
        try:
            time.sleep(30)
        except KeyboardInterrupt:
            print('Stopping logging on all sensors.')
            for s in sensors: s.stop()
            print('Done.')
            break
