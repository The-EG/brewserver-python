#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import mysql.connector as mariadb
import time
import datetime
import threading
import argparse
import configparser

from . import data

class SensorLogger:
    def __init__(self, sensor_name, db='brewserver', user='logger', interval=60):
        self.db = db
        self.db_user = user
        self.sensor_name = sensor_name
        #self.sensor = data.Sensor(sensor_name)
        self.sensor_id = self._get_sensor_id()
        self.running = False
        self.interval = interval
        
    def _dbcon(self):
        return mariadb.connect(user=self.db_user, database=self.db)
        
    def _get_sensor_id(self):
        conn = self._dbcon()
        cur = conn.cursor()
        cur.execute('SELECT id FROM sensors WHERE name = %s',(self.sensor_name,))
        for (id,) in cur:
            ret = id
        del cur
        conn.close()
        return ret
    
    def _get_last_time(self):
        conn = self._dbcon()
        cur = conn.cursor()
        cur.execute('SELECT MAX(time) FROM sensor_log WHERE sensor = %s', (self.sensor_id,))
        for (ts,) in cur:
            ret = ts
        del cur
        conn.close()
        return ret
        
    def __del__(self):
        self.stop()
        
    def log_temp(self):
        sensor = data.Sensor(self.sensor_name)
        if self._get_last_time() is not None and not sensor.last_updated.replace(microsecond=0) > self._get_last_time(): return
        
        temp = sensor.temp_f
        ts = sensor.last_updated.strftime('%Y-%m-%d %H:%M:%S')        
        sensor.close()
        conn = self._dbcon()
        
        cur = conn.cursor()
        cur.execute('INSERT INTO sensor_log (sensor, temp, time) VALUES (%s, %s, %s)', (self.sensor_id, temp, ts))
        conn.commit()
        del cur
        conn.close()        
        
    def start(self):
        self.running = True
        self.thread = threading.Thread(target=self._run)
        self.thread.start()
        
    def stop(self):
        if self.running and self.thread:
            self.running = False
            time.sleep(0.1)
            self.thread.join()
        
    def _run(self):
        last = None 
        while self.running:
            cur = datetime.datetime.now()
            if last is None or (cur - last).seconds >= self.interval:
                last = cur
                self.log_temp()
            time.sleep(1)
            

