#!/usr/bin/python
# -*- charset: utf-8 -*-
import datetime
import mysql.connector as mariadb

class BrewServerDB:
    def __init__(self, db='brewserver', user='brew-reader'):
        self.db_name = db
        self.db_usr = 'brew-reader'

    def _db_conn(self):
        return mariadb.connect(user=self.db_usr, database=self.db_name)

    def get_sensor_24hr_range(self, sensor_name):
        end = datetime.datetime.now()
        start = end - datetime.timedelta(hours=24)
        qry = (
            'SELECT MIN(l.temp) as min, MAX(l.temp) AS max '+
            'FROM sensor_log l ' +
            'JOIN sensors s ON s.id = l.sensor '+
            'WHERE s.name = %s '+
            '  AND l.time >= %s ' +
            '  AND l.time <= %s ')
        conn = self._db_conn()
        cur = conn.cursor()
        cur.execute(qry, (sensor_name, start, end))
        minmax = cur.fetchone()
        conn.close()
        return minmax

    def get_sensor_hist(self, sensor_name, start=None, end=None):
        if end is None:
            end = datetime.datetime.now()
        if start  is None:
            start = end - datetime.timedelta(hours=24)
        qry = (
            'SELECT l.time, l.temp ' +
            'FROM sensor_log l ' +
            'JOIN sensors s ON s.id = l.sensor '+
            'WHERE s.name = %s ' +
            '  AND l.time >= %s ' +
            '  AND l.time <= %s ' +
            'ORDER BY l.time ASC')
        conn = self._db_conn()
        cur = conn.cursor()
        cur.execute(qry, (sensor_name, start, end))
        rows = []

        for (ts, temp) in cur:
            rows.append({'time': ts, 'temp': temp})
        del cur
        conn.close()

        return rows

    def get_active_brew(self):
        qry = 'SELECT id, name, target_temp FROM brew_log WHERE end_ferm IS NULL ORDER BY begin_ferm DESC'
        conn = self._db_conn()
        cur = conn.cursor()
        cur.execute(qry)
        ret = []
        for (id, name, target_temp) in cur:
            ret.append({'id': id, 'name': name, 'target_temp': target_temp})
        del cur
        conn.close()
        return ret

    def get_brew_hist(self, brew_log_id):
        qry = (
            'SELECT sl.time, sl.temp '+
            'FROM brew_log bl '+
            'JOIN sensor_log sl ON sl.sensor = bl.sensor AND sl.time >= bl.begin_ferm AND (bl.end_ferm is NULL OR sl.time <= bl.end_ferm) '+
            'WHERE bl.id = %s')
        conn = self._db_conn()
        cur = conn.cursor()
        cur.execute(qry, (brew_log_id,))
        rows = []
        for (ts, temp) in cur:
            rows.append({'time': ts, 'temp': temp})
        del cur
        conn.close()

        return rows
