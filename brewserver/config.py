#!/usr/bin/python3
# -*- coding: utf-8 -*-

import configparser

class Config:
    def __init__(self, path='/mnt/pidata/brewserver/brewserver.ini'):
        self._path = path
        self.config = configparser.ConfigParser()
        self.config.read(path)

    def save(self):
        with open(self._path, 'wt') as f:
            self.config.write(f)

    def set_and_save(self, section, option, value):
        self.config.set(section, option, value)
        self.save()

    @property
    def sensor_names(self):
        sensors = []
        for section in self.config.sections():
            if section.upper().startswith('SENSOR='):
                sensors.append(section[7:])
        return sensors

    @property
    def thermostat_names(self):
        therms = []
        for section in self.config.sections():
            if section.upper().startswith('THERMOSTAT='):
                therms.append(section[11:])
        return therms

    def set_thermostat_mode(self, thermostat, mode):
        if thermostat not in self.thermostat_names:
            raise KeyError
        if mode.lower() not in ('off','cool','heat'):
            raise ValueError
        self.set_and_save(f'Thermostat={thermostat}', 'mode', mode.lower())

    def set_thermostat_target_temp(self, thermostat, temp):
        if thermostat not in self.thermostat_names:
            raise KeyError
        self.set_and_save(f'Thermostat={thermostat}', 'targetTemp', f'{temp:.0f}')
    
    def set_thermostat_limit_temp(self, thermostat, temp):
        if thermostat not in self.thermostat_names:
            raise KeyError
        self.set_and_save(f'Thermostat={thermostat}', 'limitTemp', f'{temp:.0f}')
