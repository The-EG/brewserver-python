# -*- coding: utf-8 -*-
import struct
import datetime
import posix_ipc
import mmap

# Thermostat Data Structure
#  S -  E  Sz
# ---------------------------------------------------
#  0 - 25 [26] : Target Sensor Name + NULL terminator
# 26 - 51 [26] : Limit Sensor Name + NULL terminator
# 52 - 55 [ 4] : Target Temp (float)
# 56 - 59 [ 4] : Limit Temp (float)
# 60 - 63 [ 4] : Threshold (float)
# 64 - 67 [ 4] : Last Updated (long)
# 68 - 69 [ 2] : Status (short)
# 70 - 71 [ 2] : Mode (short)

class Thermostat:
    def __init__(self, name):
        self.name = name
        mem = posix_ipc.SharedMemory(f"/brewserver_therm_{self.name}_data")
        self._mem = mmap.mmap(mem.fd, 0)
        mem.close_fd()
        self._sem = posix_ipc.Semaphore(f"/brewserver_therm_{self.name}")

    def close(self):
        self._mem.close()
        self._sem.close()

    def _get_bytes(self, offset, n):
        self._sem.acquire()
        self._mem.seek(offset)
        ret = self._mem.read(n)
        self._sem.release()
        return ret

    def _set_bytes(self, data, offset):
        self._sem.acquire()
        self._mem.seek(offset)
        self._mem.write(data)
        self._sem.release()

    @property
    def target_sensor(self):
        return self._get_bytes(0,26).decode('ascii').strip('\x00')

    @target_sensor.setter
    def target_sensor(self, value):
        v = value
        if len(value) > 25: v = v[:25]
        self._sem.acquire()
        self._mem.seek(0)
        self._mem.write(b'\x00'*26)        # null out the buffer
        self._mem.write(v.encode('ascii')) # then write the string to it
        self._sem.release()

    @property
    def limit_sensor(self):
        return self._get_bytes(26,26).decode('ascii').strip('\x00')

    @limit_sensor.setter
    def limit_sensor(self, value):
        v = value
        if len(value) > 25: v = v[:25]
        self._sem.acquire()
        self._mem.seek(26)
        self._mem.write(b'\x00'*26)         # null out the buffer
        self._mem.write(v.encode('ascii'))  # then write the string to it
        self._sem.release()

    @property
    def target_temp(self):
        return struct.unpack("f", self._get_bytes(52,4))[0]

    @target_temp.setter
    def target_temp(self, value):
        self._set_bytes(struct.pack("f", value), 52)

    @property
    def limit_temp(self):
        return struct.unpack("f", self._get_bytes(56,4))[0]

    @limit_temp.setter
    def limit_temp(self, value):
        self._set_bytes(struct.pack("f", value), 56)

    @property
    def threshold(self):
        return struct.unpack("f", self._get_bytes(60,4))[0]

    @threshold.setter
    def threshold(self, value):
        self._set_bytes(struct.pack("f", value), 60)

    @property
    def last_updated(self):
        t = struct.unpack("l", self._get_bytes(64,4))[0]
        return datetime.datetime.fromtimestamp(t)

    @property
    def status(self):
        s = struct.unpack("H", self._get_bytes(68, 2))[0]
        if s==0: return "OFF"
        elif s==1: return "ON"
        elif s==2: return "WAIT"
        return None

    @property
    def mode(self):
        m = struct.unpack("H", self._get_bytes(70, 2))[0]
        if m==0: return 'OFF'
        elif m==1: return "COOL"
        elif m==2: return "HEAT"
        return None

    @mode.setter
    def mode(self, value):
        m = -1
        if value.upper()=="OFF":
            m = 0
        elif value.upper()=="COOL":
            m = 1
        elif value.upper()=="HEAT":
            m = 2
        else:
            print("mode must be OFF, COOL or HEAT")
            return
        self._set_bytes(struct.pack("H", m), 70)

# Sensor Data Structure
#  S -  E  Sz
# ---------------------------------------------------
#  0 -  3 [ 4] : Temp (C) (float)
#  4 -  7 [ 4] : Last Updated (long)

class Sensor:
    def __init__(self, name):
        self.name = name
        mem = posix_ipc.SharedMemory(f"/brewserver_sensor_{self.name}_data")
        self._mem = mmap.mmap(mem.fd,0)
        mem.close_fd()
        self._sem = posix_ipc.Semaphore(f"/brewserver_sensor_{self.name}")

    def close(self):
        self._mem.close()
        self._sem.close()

    def _get_bytes(self, offset, n):
        self._sem.acquire()
        self._mem.seek(offset)
        ret = self._mem.read(n)
        self._sem.release()
        return ret
    
    @property
    def temp_c(self):
        return struct.unpack("f", self._get_bytes(0, 4))[0]

    @property
    def temp_f(self):
        return self.temp_c * (9.0 / 5.0) + 32.0    

    @property
    def last_updated(self):
        t = struct.unpack("l", self._get_bytes(4,4))[0]
        return datetime.datetime.fromtimestamp(t)
