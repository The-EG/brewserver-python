# The-EG's Brewserver

The Brewserver is a Raspberry Pi 3B that monitors and controls a fermentation environment (fancy term for a chest freezer) for fermenting home brewed beer.

## Brewserver-Python

This repository houses Python code and utilities for interacting with the [Brewserver library and service control](https://gitlab.com/The-EG/brewserver-core):
 - Thermostat Control (read and write)
 - Sensor Monitoring (read only)
 - Configuration
 - Logger - temperature logging to MariaDB
 - Webserver - REST api serving access to Thermostat Control, Sensor Monitoring and Configuration


## Brewserver-Logger Service Installation

- Edit the `ExecStart` value of services/brewserver-logger.service to the appropriate path of run_loggers.py
- Copy brewserver-logger.service to the systemd services folder: `sudo cp services/brewserver-logger.service /etc/systemd/system/`
- Tell systemd to reload unit files: `sudo systemctl daemon-reload`
- Enable the service so it starts automatically on boot: `sudo systemctl enable brewserver-logger`
- Start the service: `sudo systemctl start brewserver-logger`
- Optionally, check that it worked: `sudo systemctl status brewserver-logger`

## Brewserver-Web Service Installation

Very similar to the logger service:
- Edit the `WorkingDirectory` value of services/brewserver-web.service to the appropriate path of this repository
- Edit the `User` value of services/brewserver-web.service to a non-root user
- Copy brewserver-web.service to the systemd services folder: `sudo cp services/brewserver-web.service /etc/systemd/system/`
- Tell systemd to reload unit files: `sudo systemctl daemon-reload`
- Enable the service so it starts automatically on boot: `sudo systemctl enable brewserver-web`
- Start the service: `sudo systemctl start brewserver-web`
- Optionally, check that it worked: `sudo systemctl status brewserver-web`